<%@ include file="/WEB-INF/public/common/jsp_defined.jsp"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- saved from url=(0039)https://console.ustack.com/uos/overview -->
<html lang="zh"><head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<style id="avalonStyle">.fixMsIfFlicker{ display: none!important }</style>
        <meta charset="utf-8">
        <meta name="viewport" content="width=1280">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta name="renderer" content="webkit">
        <title>新网 Cloud Console中心</title>
        <link rel="shortcut icon" href="https://console.ustack.com/static/public/favicon.ico" type="image/x-icon">
        <script>window.UOS = {"socket": {"receiveTopic": "event", "sendTopic": "message", "address": "http://localhost:4000"}, "defaultApp": "aaf2", "proxyBase": "/api/proxy", "t": {"TEST": "\u6d4b\u8bd5 i18n \u6587\u6848", "LANG": "zh-CN", "ticket": "\u652f\u6301", "sMessage": {"count": 2}, "user": {"menu": {"sign_out": "\u6ce8\u9500", "switch_to_admin": "\u7cfb\u7edf\u6743\u9650: ", "swtich_language": {"languages": [{"field": "zh-CN", "label": "\u7b80\u4f53\u4e2d\u6587"}, {"field": "en-US", "label": "English"}], "label": "\u8bed\u8a00: "}}}}, "appList": "/app_list", "user": {"name": "zhoutanxin", "roles": [{"id": "dd1585c29bc5426683f027ed19f34261", "name": "domain_admin"}], "projectid": "fbebb0cab0304f5ca02326f433f892b5", "gravatar_url": "https://www.gravatar.com/avatar/73a893848a990615a28dba716b32f347", "email": "zhoutanxin@hotmail.com", "projectname": "zhoutanxin_project", "country_code": "86", "mobile_number": "13683027377", "id": "28139679fd0c4b45a91a46cff6a07e2d"}} ; t = UOS.t;</script>
        <script src="<c:url value="${ctx }/js/require.min.js" />"></script>
        
        <link rel="stylesheet" href="${ctx}/css/5fb95fa.lemon.css">
        <script src="${ctx }/js/5fb95fa.main.min.js"></script>
         
    <style type="text/css" rel="stylesheet">.slickgrid_721553 .slick-header-column { left: 1000px; } .slickgrid_721553 .slick-top-panel { height:25px; } .slickgrid_721553 .slick-headerrow-columns { height:32px; } .slickgrid_721553 .slick-cell { height:23px; } .slickgrid_721553 .slick-row { height:41px; } .slickgrid_721553 .l0 { } .slickgrid_721553 .r0 { } .slickgrid_721553 .l1 { } .slickgrid_721553 .r1 { } .slickgrid_721553 .l2 { } .slickgrid_721553 .r2 { } .slickgrid_721553 .l3 { } .slickgrid_721553 .r3 { } .slickgrid_721553 .l4 { } .slickgrid_721553 .r4 { } .slickgrid_721553 .l5 { } .slickgrid_721553 .r5 { } .slickgrid_721553 .l6 { } .slickgrid_721553 .r6 { } .slickgrid_721553 .l7 { } .slickgrid_721553 .r7 { }</style></head>
    <body>
        <div class="main">
        <!-- #header开始 -->
            <jsp:include page="/v_top"></jsp:include>
            <!-- #header -->
            <div class="main_content">
                <div class="panel_center_wrapper" id="panel_center_wrapper">
                	<!-- #左边sidebar开始 -->
                    <jsp:include page="/v_left"></jsp:include>
                    <!-- #左边sidebar结束 -->
                    <!-- 右边用用区开始 -->
                    <div class="appArea" wp-type="view">
                    <jsp:include page="/v_right"></jsp:include>
                    </div>
					<!-- 右边应用区结束 -->
                </div><!-- .panel_center_wrapper -->
            </div><!-- .main_content -->
        </div><!-- #main -->
        <div class="appmodal fadeOut" style=""></div>
    

</body></html>