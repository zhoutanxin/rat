<%@ include file="/WEB-INF/public/common/jsp_defined.jsp"  %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>  
<%@taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<style id="avalonStyle">.fixMsIfFlicker{ display: none!important }</style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <title>新网控制台登陆</title>
  <link rel="shortcut icon" href="https://console.ustack.com/static/public/favicon.ico" type="image/x-icon">
  <style type="text/css">
    html,body{margin:0;padding:0;width:100%;height:100%;overflow:hidden}body{background:#00243d url(/static/public/img/bg.jpg) center center no-repeat fixed;font-family:"Helvetica Neue",Helvetica,Arial,"Microsoft YaHei",sans-serif;font-size:12px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;-webkit-tap-highlight-color:transparent}input{-webkit-transition-duration:.2s;-moz-transition-duration:.2s;-o-transition-duration:.2s;transition-duration:.2s;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}::-webkit-input-placeholder{color:#a4bfd7}::-moz-placeholder{color:#a4bfd7}:-ms-input-placeholder{color:#a4bfd7}input:-moz-placeholder{color:#a4bfd7}a{text-decoration:none;color:#00bdd8}a:hover,a:active{text-decoration:underline;color:#00bdd8}.wrapper{display:-ms-flexbox;display:-webkit-flex;display:-moz-flex;display:-ms-flex;display:flex;-webkit-flex-direction:column;-moz-flex-direction:column;-ms-flex-direction:column;flex-direction:column;width:100%;height:100%}.wrapper .before{-webkit-flex:1;-moz-flex:1;-ms-flex:1;flex:1}.wrapper .after{-webkit-flex:3;-moz-flex:3;-ms-flex:3;flex:3}.con{-webkit-flex:1;-moz-flex:1;-ms-flex:1;flex:1;margin:auto;width:302px;height:444px;min-height:444px;max-height:444px}.con .logo{width:116px;height:116px;background:url(/static/public/img/login-logo.png) 0 0 no-repeat;margin:0 auto}.email,.password,.cap-wrapper input[type="text"]{display:inline-block;padding-left:16px;width:302px;height:44px;font-size:14px;border:1px solid #9da4ae;border-radius:2px;background-color:rgba(255,255,255,.08);outline:0;color:#fff}.cap-wrapper{margin-top:20px}.cap-wrapper input[type="text"]{width:188px}img{float:right;width:90px;height:44px;vertical-align:middle;cursor:pointer}.email:hover,.email:focus,.password:hover,.password:focus,.cap-wrapper input[type='text']:hover,.cap-wrapper input[type='text']:focus{background-color:rgba(0,175,200,.08);border:1px solid #50c5d6}.email.error,.password.error,.cap-wrapper input[type='text'].error{background-color:rgba(220,85,101,.08);border:1px solid #dc5565}.email{margin-top:40px}.password{margin-top:20px}.tip{height:40px;line-height:40px;color:#00bdd8}.tip .error{float:left;color:#dc5565}.tip .retrieve{float:right}.submit{display:block;padding:0;width:302px;height:44px;border:0;border-radius:2px;background-color:#00b4d2;font-size:16px;color:#fff;text-align:center;cursor:pointer;-webkit-appearance:none}.submit:hover{background-color:#00a5c1}.submit:active{background-color:#019cb4}.signup{margin-top:16px;color:#cee1f2}@media(-webkit-min-device-pixel-ratio:2),(min-resolution:192dpi),(min-resolution:2dppx){.con .logo{ background-image:url(/static/public/img/login-logo@2x.png);background-size:116px 116px}
  </style>
</head>
<body class="bg">
  <div class="wrapper">
    <div class="before"></div>
    <div class="con">
      <div class="logo"></div>
      
      <form method="POST" action="${ctx }login">
          
          <input name="userName" id="id_email" type="text" autofocus="autofocus" placeholder="邮箱" class="email" tabindex="1">
          <input name="pwd" id="id_password" placeholder="密码" type="password" class="password" tabindex="2">
           
        <div class="tip">
            
                
            
            <div class="retrieve">
                <a href="http://ustack.com/accounts/password/reset/" target="_blank">忘记密码?</a><spring:message code="user.name"></spring:message>
            </div>
        </div>
        <input type="submit" class="submit" value="登录">
      </form>
      <div class="signup">
          没有账号? ${info }
          <form:form commandName="user" cssStyle="color:red">
		        <!-- 输出所有的错误信息 
		        -->
			    <form:errors path="*" />
			    <!-- 输出单个错误信息 
			    <form:errors path="pwd" />
			    -->
          </form:form>
          <a href="http://ustack.com/accounts/register/" target="_blank">
              注册
          </a>
      </div>
    
    </div>
    <div class="after"></div>
  </div>
  <script async="" src="${ctx }js/analytics.js"></script><script>
    (function(win,doc){doc.forms[0].onsubmit=function(){var email_dom=doc.getElementById("id_email"),pwd_dom=doc.getElementById("id_password"),email=email_dom.value,pwd=pwd_dom.value,len=pwd.length,reg=/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;var cap_dom=doc.getElementById("id_captcha_1");if(cap_dom){var cap=cap_dom.value;var cap_len=cap.length}if(email.match(reg)&&len>0&&(!cap_dom||cap_len>0)){return true}else{if(email.match(reg)){email_dom.classList.remove("error")}else{email_dom.classList.add("error")}if(len>0){pwd_dom.classList.remove("error")}else{pwd_dom.classList.add("error")}if(cap_dom){if(cap_len>0){cap_dom.classList.remove("error")}else{cap_dom.classList.add("error")}}return false}};var img=doc.querySelector(".cap-wrapper img");var hidden_input=doc.getElementById("id_captcha_0");if(img){img.onclick=function(){var xhr=new XMLHttpRequest;xhr.open("GET","/refresh_captcha/",true);xhr.send(null);xhr.onreadystatechange=function(){if(xhr.readyState===4&&xhr.status===200){var url=xhr.responseText;img.setAttribute("src",url);hidden_input.setAttribute("value",url.split("/")[3])}}}}})(window,document);

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-51240946-1', 'auto');
  ga('send', 'pageview');

  </script>
  <script src="${ctx }js/require.min.js" defer=""></script>

  
  
  <script src="<c:url value="${ctx }js/5fb95fa.main.min.js" />" defer=""></script>
  


</body></html>