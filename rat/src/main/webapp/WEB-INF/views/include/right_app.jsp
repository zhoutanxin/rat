<%@ include file="/WEB-INF/public/common/jsp_defined.jsp"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<div id="aaf2" wp-type="app" class="">
<div class="uosApp instanceApp">
<div class="pagesWp">
<div class="uosPage" data-field="overview" id="uoscloudoverview" style="display: block;">
<div class="vmPage overview-page">
<div class="wrapper">
    <div class="tip hide">您的账户余额大约还能支持<span> <strong>-1</strong> 天</span>，请及时<a href="https://console.ustack.com/uos/account_charge" wp-type="link">充值</a>。</div>
    <div class="warn-tip tip hide">您的账户已欠费，请及时<a href="https://console.ustack.com/uos/account_charge" wp-type="link">充值</a>。</div>
</div>
<div class="wrapper">
    <div class="balance">
        <h3>账户余额</h3>
        <div class="general">
            <div>
                <span class="unit"></span>
                <span class="price">10</span>
                <span class="unit">元</span>
            </div>
            <div>
                <a href="https://console.ustack.com/uos/record_overview" wp-type="link">消费记录</a>
                <a href="https://console.ustack.com/uos/account_charge" wp-type="link">账户充值</a>
            </div>
        </div>
        <div class="instance">
            <a href="javascript:;">创建云主机</a>
        </div>
        <div class="topology">
            <a href="https://console.ustack.com/uos/topology" wp-type="link">网络拓扑</a>
        </div>
    </div>
    <div class="quota">
        <h3>资源配额</h3>
        <a href="javascript:;">查看更多配额</a>
        <ul class="clearfix">
            <li>
                <a wp-type="link" href="https://console.ustack.com/uos/instance">
                    <div class="circle">
                        <canvas width="236" height="236" id="J_quota_instances"></canvas>
                        <div class="num blue"><span class="gray">0</span><span class="">/10</span></div>
                    </div>
                </a>
                <div class="legend">云主机</div>
            </li>
        
            <li>
                <a wp-type="link" href="https://console.ustack.com/uos/ip">
                    <div class="circle">
                        <canvas width="236" height="236" id="J_quota_floating_ips"></canvas>
                        <div class="num blue"><span class="gray">0</span><span class="">/3</span></div>
                    </div>
                </a>
                <div class="legend">公网IP</div>
            </li>
        
            <li>
                <a wp-type="link" href="https://console.ustack.com/uos/volume">
                    <div class="circle">
                        <canvas width="236" height="236" id="J_quota_volumes"></canvas>
                        <div class="num blue"><span class="gray">0</span><span class="">/10</span></div>
                    </div>
                </a>
                <div class="legend">云硬盘</div>
            </li>
        
            <li>
                <a wp-type="link" href="https://console.ustack.com/uos/snapshot">
                    <div class="circle">
                        <canvas width="236" height="236" id="J_quota_snapshots"></canvas>
                        <div class="num blue"><span class="gray">0</span><span class="">/10</span></div>
                    </div>
                </a>
                <div class="legend">硬盘快照</div>
            </li>
        
            <li>
                <a wp-type="link" href="https://console.ustack.com/uos/router">
                    <div class="circle">
                        <canvas width="236" height="236" id="J_quota_routers"></canvas>
                        <div class="num blue"><span class="gray">0</span><span class="">/3</span></div>
                    </div>
                </a>
                <div class="legend">路由器</div>
            </li>
        
            <li>
                <a wp-type="link" href="https://console.ustack.com/uos/network">
                    <div class="circle">
                        <canvas width="236" height="236" id="J_quota_networks"></canvas>
                        <div class="num blue"><span class="gray">0</span><span class="">/3</span></div>
                    </div>
                </a>
                <div class="legend">私有网络</div>
            </li>
        
            <li>
                <a wp-type="link" href="https://console.ustack.com/uos/security">
                    <div class="circle">
                        <canvas width="236" height="236" id="J_quota_security_groups"></canvas>
                        <div class="num blue"><span class="">1</span><span class="">/10</span></div>
                    </div>
                </a>
                <div class="legend">安全组</div>
            </li>
        
            <li>
                <a wp-type="link" href="https://console.ustack.com/uos/key-pair">
                    <div class="circle">
                        <canvas width="236" height="236" id="J_quota_key_pairs"></canvas>
                        <div class="num blue"><span class="gray">0</span><span class="">/100</span></div>
                    </div>
                </a>
                <div class="legend">密钥对</div>
            </li>
        </ul>
    </div>
</div>
<div class="wrapper">
    <div class="news">
        <h3>最新消息</h3>
        <ul>
            <li>- UOS云服务开始邀请测试，有任何问题欢迎提交<a href="https://console.ustack.com/uos/ticket" wp-type="link">工单支持</a>反馈</li>
            <li>- UOS文档中心上线：<a href="http://docs.ustack.com/" target="_blank">docs.ustack.com</a></li>
        </ul>
    </div>
    <div class="operation">
        <a href="https://console.ustack.com/uos/operations" wp-type="link" class="viewMore">查看更多</a>
        <h3>最近操作</h3>
        <div class="operationview">
            <div class="table-header">
                <div>资源名称</div>
            
                <div>操作</div>
            
                <div>状态</div>
            
                <div>操作时长</div>
            
                <div>操作时间</div>
            </div>
            <div class="table">
                <div class="table-content order-table"></div>
                <div class="summary">
                    <div class="page">
                        <span>页数: 1/0, 总计: 0</span>
                        <a href="javascript:;" class="prev disabled">&nbsp;</a>
                        <span class=""></span>
                        <a href="javascript:;" class="next disabled">&nbsp;</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div></div><div class="uosPage" data-field="instance" id="uoscloudinstance" style="display: none;"><div class="vmPage page">
    <!--ms-if-->
    <!--ms-if-->
    <div class="pageHeader">
        <a href="javascript:;" wp-type="button" class="create action_create">创建云主机</a>
        <ul class="btnList">
            <li><a href="javascript:;" class="disabled" field="vnc_console">VNC登录</a></li>
        
            <li><a href="javascript:;" class="disabled" field="start">开机</a></li>
        
            <li><a href="javascript:;" class="disabled" field="stop">关机</a></li>
        </ul>
        <div class="moreActions"><a href="javascript:;" wp-type="droptdown" class="more_actions">更多</a></div>
        <div class="searchDiv">
            <form method="get" action=""><input type="text" class="searchInput" placeholder="搜索"><a href="javascript:;" class="doSearch"></a></form>
        </div>
        <a href="javascript:;" class="iconRefresh"></a>
    </div>
    <div class="dataView slickgrid_721553 ui-widget" style="overflow: hidden; outline: 0px; position: relative;"><div tabindex="0" hidefocus="" style="position:fixed;width:0;height:0;top:0;left:0;outline:0;"></div><div class="slick-header ui-state-default" style="overflow:hidden;position:relative;"><div class="slick-header-columns" style="left: -1000px; width: 2222px;" unselectable="on"><div class="ui-state-default slick-header-column" id="slickgrid_721553mschecked" title="" style="width: 30px;"><span class="slick-column-name"><input type="checkbox" class="selectAll"></span></div><div class="ui-state-default slick-header-column slick-header-sortable" id="slickgrid_721553name" title="" style="width: 159px;"><span class="slick-column-name">名称</span><span class="slick-sort-indicator"></span><div class="slick-resizable-handle"></div></div><div class="ui-state-default slick-header-column slick-header-sortable" id="slickgrid_721553addresses" title="" style="width: 159px;"><span class="slick-column-name">内网IP</span><span class="slick-sort-indicator"></span><div class="slick-resizable-handle"></div></div><div class="ui-state-default slick-header-column slick-header-sortable" id="slickgrid_721553floating_ip" title="" style="width: 159px;"><span class="slick-column-name">公网IP</span><span class="slick-sort-indicator"></span><div class="slick-resizable-handle"></div></div><div class="ui-state-default slick-header-column slick-header-sortable" id="slickgrid_721553image" title="" style="width: 159px;"><span class="slick-column-name">镜像</span><span class="slick-sort-indicator"></span><div class="slick-resizable-handle"></div></div><div class="ui-state-default slick-header-column slick-header-sortable" id="slickgrid_721553flavor" title="" style="width: 159px;"><span class="slick-column-name">配置<span class="filterWp"><span class="selectWrapper"><span class="selectLabel"></span><select name="flavor" class="styleDone" style="opacity: 0;"><option value="-1">ALL</option><option value="215ea357-2d56-4387-9991-0c083806e3f8">compute-12</option><option value="b59d6ef7-95dd-424d-9db5-a2bc9c792f73">compute-2</option><option value="c40bf599-a979-4404-8332-5a23f75a66cd">compute-4</option><option value="270a6d65-d1da-4a15-ad3a-7bcf969839e5">compute-8</option><option value="d049a545-6f64-49d0-81d2-68a3e11f656f">memory-1</option><option value="21c0cf18-d0dc-43ab-8fdb-40ba8b0935d9">memory-2</option><option value="b59c48a8-f816-4689-8f0c-2951d37e9c44">memory-4</option><option value="aa5987fe-6bf4-4162-a817-5cdc5c4338db">memory-8</option><option value="8abaa0f9-30e1-4509-8ba5-5c97366029df">micro-1</option><option value="3332c026-533a-43d0-b415-abd3fdab09bf">micro-2</option><option value="8e7ae928-b7ee-4883-b2b5-49d23e0b1909">standard-1</option><option value="8aaf699a-c760-43cf-8241-4b4cabbfaa51">standard-12</option><option value="7f05780f-caf6-43bd-af55-b183cec6a758">standard-2</option><option value="be7f5c4e-08c2-4576-8bac-290d2a9335f5">standard-4</option><option value="e15253c2-c340-4402-89ae-cfd794774560">standard-8</option></select></span></span></span><span class="slick-sort-indicator"></span><div class="slick-resizable-handle"></div></div><div class="ui-state-default slick-header-column slick-header-sortable" id="slickgrid_721553status" title="" style="width: 158px;"><span class="slick-column-name">状态<span class="filterWp"><span class="selectWrapper"><span class="selectLabel"></span><select name="status" class="styleDone" style="opacity: 0;"><option value="-1">ALL</option></select></span></span></span><span class="slick-sort-indicator"></span><div class="slick-resizable-handle"></div></div><div class="ui-state-default slick-header-column slick-header-sortable" id="slickgrid_721553created" title="" style="width: 158px;"><span class="slick-column-name">创建时间</span><span class="slick-sort-indicator"></span></div></div></div><div class="slick-headerrow ui-state-default" style="overflow: hidden; position: relative; display: none;"><div class="slick-headerrow-columns" style="width: 1205px;"></div><div style="display: block; height: 1px; position: absolute; top: 0px; left: 0px; width: 1205px;"></div></div><div class="slick-top-panel-scroller ui-state-default" style="overflow: hidden; position: relative; display: none;"><div class="slick-top-panel" style="width:10000px"></div></div><div class="slick-viewport" style="width: 100%; overflow: auto; outline: 0px; position: relative; height: 603px;"><div class="grid-canvas" style="width: 1205px; height: 586px;"></div></div><div tabindex="0" hidefocus="" style="position:fixed;width:0;height:0;top:0;left:0;outline:0;"></div></div>
    <div class="noDataView">没有云主机 ，立即<a href="javascript:;" class="mockCreate">创建</a>。</div>
    <div class="loading"><div class="spinInner"></div></div>
    <div class="detailView">
        <div class="vdivider"></div>
        <a href="javascript:;" class="closeDetail"></a>
        <div class="tabs"><a href="javascript:;" class="tab selected" field="description">详细信息</a><a href="javascript:;" class="tab" field="monitor">监控</a><a href="javascript:;" class="tab" field="system_log">屏幕输出</a><a href="javascript:;" class="tab" field="vnc">VNC登录</a><a href="javascript:;" class="tab" field="topology">拓扑</a></div>
        <div class="tabPanels">
            <div class="panel" field="description">
                <div class="leftColumn">
                    <div class="row"><label>名称:</label><span></span></div>
                
                    <div class="row"><label>UUID:</label><span></span></div>
                
                    <div class="row"><label>公网IP:</label><span></span></div>
                
                    <div class="row"><label>镜像:</label><span></span></div>
                
                    <div class="row"><label>安全组:</label><span></span></div>
                
                    <div class="row"><label>密钥:</label><span></span></div>
                
                    <div class="row"><label>云硬盘:</label><span></span></div>
                
                    <div class="row"><label>状态:</label><span></span></div>
                
                    <div class="row"><label>创建时间:</label><span></span></div>
                </div>
                <!--ms-if-->
                <div class="logPanel"></div>
            </div>
        
            <div class="panel hide" field="monitor">
                <!--ms-if-->
                <div class="monitorPanel">
                    <div class="topBar">
                        <ul class="monitorSelector">
                            <li><a href="javascript:;" value="3h" class="selected">最近三小时</a></li>
                        
                            <li><a href="javascript:;" value="1d">最近一天</a></li>
                        
                            <li><a href="javascript:;" value="7d">最近一周</a></li>
                        
                            <li><a href="javascript:;" value="30d">最近一个月</a></li>
                        
                            <li><a href="javascript:;" value="real">实时监控</a></li>
                        </ul>
                    </div>
                    <div class="monitorContainer"></div>
                </div>
                <div class="logPanel"></div>
            </div>
        
            <div class="panel hide" field="system_log">
                <!--ms-if-->
                <!--ms-if-->
                <div class="logPanel"></div>
            </div>
        
            <div class="panel hide" field="vnc">
                <!--ms-if-->
                <!--ms-if-->
                <div class="logPanel"></div>
            </div>
        
            <div class="panel hide" field="topology">
                <!--ms-if-->
                <!--ms-if-->
                <div class="logPanel"></div>
            </div>
        </div>
    </div>
    <div class="detailMoreActionDropDown hide"><div><ul><li><a href="javascript:;" class="menubutton" field="vnc_console">VNC登录</a></li><li><a href="javascript:;" class="menubutton" field="create_image">创建主机快照</a></li></ul></div><div><ul><li><a href="javascript:;" class="menubutton" field="start">开机</a></li><li><a href="javascript:;" class="menubutton" field="reboot">重启</a></li><li><a href="javascript:;" class="menubutton" field="suspend">休眠</a></li><li><a href="javascript:;" class="menubutton" field="resume">恢复</a></li><li><a href="javascript:;" class="menubutton" field="stop">关机</a></li><li><a href="javascript:;" class="menubutton" field="delete">删除</a></li></ul></div><div><ul><li><a href="javascript:;" class="menubutton" field="associate_public_ip">绑定公网IP</a></li><li><a href="javascript:;" class="menubutton" field="dissociate_public_ip">解除公网IP</a></li><li><a href="javascript:;" class="menubutton" field="join_networks">加入网络</a></li><li><a href="javascript:;" class="menubutton" field="leave_networks">离开网络</a></li></ul></div><div><ul><li><a href="javascript:;" class="menubutton" field="add_secruity_group">修改安全组</a></li><li><a href="javascript:;" class="menubutton" field="change_password">修改密码</a></li><li><a href="javascript:;" class="menubutton" field="add_key_pairs">修改SSH密钥</a></li></ul></div><div><ul><li><a href="javascript:;" class="menubutton" field="add_volume">挂载云硬盘</a></li><li><a href="javascript:;" class="menubutton" field="delete_volume">卸载云硬盘</a></li></ul></div></div>
    <div class="moreActionDropDown hide">
        <div>
            <h4 class="">云主机管理</h4>
            <ul class="">
                <li><a href="javascript:;" class="menubutton disabled" field="reboot">重启</a></li>
            
                <li><a href="javascript:;" class="menubutton disabled" field="create_image">创建主机快照</a></li>
            
                <li><a href="javascript:;" class="menubutton disabled" field="resize">更改配置</a></li>
            </ul>
        </div>
    
        <div>
            <h4 class="">网络管理</h4>
            <ul class="">
                <li><a href="javascript:;" class="menubutton disabled" field="associate_public_ip">绑定公网IP</a></li>
            
                <li><a href="javascript:;" class="menubutton disabled" field="dissociate_public_ip">解除公网IP</a></li>
            
                <li><a href="javascript:;" class="menubutton disabled" field="join_networks">加入网络</a></li>
            </ul>
        </div>
    
        <div>
            <h4 class="">网络管理</h4>
            <ul class="">
                <li><a href="javascript:;" class="menubutton disabled" field="add_secruity_group">修改安全组</a></li>
            
                <li><a href="javascript:;" class="menubutton disabled" field="change_password">修改密码</a></li>
            
                <li><a href="javascript:;" class="menubutton disabled" field="add_key_pairs">修改SSH密钥</a></li>
            </ul>
        </div>
    
        <div>
            <h4 class="">存储管理</h4>
            <ul class="">
                <li><a href="javascript:;" class="menubutton disabled" field="add_volume">挂载云硬盘</a></li>
            
                <li><a href="javascript:;" class="menubutton disabled" field="delete_volume">卸载云硬盘</a></li>
            </ul>
        </div>
    
        <div>
            <h4 class="">电源管理</h4>
            <ul class="">
                <li><a href="javascript:;" class="menubutton disabled" field="delete">删除</a></li>
            </ul>
        </div>
    </div>
</div></div><div class="uosPage" data-field="image" id="uoscloudimage"></div><div class="uosPage" data-field="app-image" id="uoscloudapp-image"></div><div class="uosPage" data-field="snap-image" id="uoscloudsnap-image"></div><div class="uosPage" data-field="lb" id="uoscloudlb"></div><div class="uosPage" data-field="interface" id="uoscloudinterface"></div><div class="uosPage" data-field="router" id="uoscloudrouter"></div><div class="uosPage" data-field="network" id="uoscloudnetwork"></div><div class="uosPage" data-field="subnet" id="uoscloudsubnet"></div><div class="uosPage" data-field="key-pair" id="uoscloudkey-pair"></div><div class="uosPage" data-field="ip" id="uoscloudip"></div><div class="uosPage" data-field="security" id="uoscloudsecurity"></div><div class="uosPage" data-field="volume" id="uoscloudvolume"></div><div class="uosPage" data-field="snapshot" id="uoscloudsnapshot"></div><div class="uosPage" data-field="vpn" id="uoscloudvpn"></div><div class="uosPage" data-field="ticket" id="uoscloudticket"></div><div class="uosPage" data-field="topology" id="uoscloudtopology" style="display: none;"><div class="topology vmPage"><div class="pageHeader"><div class="toggles"><a href="https://console.ustack.com/uos/topology/vdc" wp-type="link" class="toggler">私有网络</a><a href="https://console.ustack.com/uos/topology/shared" wp-type="link" class="toggler selected">基础网络</a></div><div class="loading hide"></div><a href="javascript:;" wp-type="button" class="action_create" field="create_router" style="display: none;">创建路由器</a><a href="javascript:;" wp-type="button" class="action_create" field="create_network" style="display: none;">创建私有网络</a><a href="javascript:;" wp-type="button" class="action_create" field="create_instance">创建云主机</a><a href="javascript:;" class="iconRefresh"></a></div><div class="topologyCanvas"><div class="publicFow" style="left: 605.5px; display: block;"></div><div class="topologyCanvasInner" style="height: 457px; width: 1211px;"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="1211" height="457"><desc>Created with Highcharts 3.0.9</desc><defs></defs><rect rx="2" ry="2" fill="#59cbdb" x="0" y="0" width="1211" height="5"></rect><g zIndex="2" transform="translate(0, 167)" type="network"><rect rx="2" ry="2" fill="rgb(231,249,251)" x="0" y="0" width="1211" height="28" fill-opacity="0.9" data-fill="rgba(231,249,251, 0.9)" data-hover="rgba(64,197,235, 0.9)"></rect><image preserveAspectRatio="none" x="10" y="7" width="0" height="0" href="/static/modules/apps/instance/i/topology/icon-add.png" data-id="add" data-type="button" style="cursor:pointer;"></image><image preserveAspectRatio="none" x="10" y="7" width="0" height="0" href="/static/modules/apps/instance/i/topology/icon-remove.png" data-id="remove" data-type="button" style="cursor:pointer;"></image><g type="network" style="cursor:pointer;"><image preserveAspectRatio="none" x="10" y="10" width="16" height="11" href="/static/modules/apps/instance/i/icon-network.png"></image><text x="30" y="20" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, Arial, Helvetica, sans-serif;font-size:12px;" zIndex="1" fontSize="12" fontFamily="Arial, Helvetica" fill="#252f3d"><tspan x="30">基础网络</tspan></text></g></g><path fill="none" d="M 605.5 0 605.5 197" stroke="#59cbdb" stroke-width="2" zIndex="100"></path></svg></div></div><div class="loading"><div class="spinInner"></div></div><div class="flowoute" style="display: none;"><h1></h1><div class="content"></div><div class="buttons"></div></div></div></div><div class="uosPage" data-field="deprecated" id="uosclouddeprecated"></div></div><div class="loadingIndicator"></div></div></div><div id="aaf3" wp-type="app" class="hide"></div>
