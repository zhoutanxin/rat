<%@ include file="/WEB-INF/public/common/jsp_defined.jsp"%>
<%@ page language="java" pageEncoding="UTF-8"%>
		<div id="header">
                <div class="navbar">
                    <div class="logo">
                        <a href="http://ustack.com/"></a>
                    </div>
                    <div class="user-info">
                        <div class="region">Beijing</div>
                        <div class="profile">
                            <a href="javascript:;">
                                <img src="${ctx }/js/73a893848a990615a28dba716b32f347">
                                <span>zhoutanxin</span>
                                <span class="arrow"></span>
                            </a>
                            <div class="account">
                                <ul>
                                    <li><a href="https://console.ustack.com/uos/account_charge" wp-type="link">我的账户</a></li>
                                    <li><a href="https://console.ustack.com/uos/setting" wp-type="link">个人设置</a></li>
                                    <li class="lang">
                                        <a href="javascript:;" class="english
                                            " lang="en-US">English</a>
                                            <span>|</span>
                                        <a href="javascript:;" class="chinese
                                            selected" lang="zh-CN">中文</a>
                                    </li>
                                    <li><a href="${ctx }v_login" title="注销">注销</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
