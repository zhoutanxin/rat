<%@ include file="/WEB-INF/public/common/jsp_defined.jsp"%>
<%@ page language="java" pageEncoding="UTF-8"%>
  		<script src="<c:url value="${ctx }js/jquery-1.5.1.min.js"></c:url>"></script>
  				
		<script type="text/javascript">
			function invoke(req) {
				if($(".appArea").data(req) ){
					$(".appArea").empty();
					$(".appArea").append($(".appArea").data(req) );
				}else{
					$.ajax({
						url: req,
						type: "post",
						data: { name: "ajax" },//如果请求的自身页面，为了在后台判断是不是ajax请求
						error: function(xhr, textStatus, errorThown) {
							alert(errorThown);
						},
						success: function(data) {
							$(".appArea").empty();
							$(".appArea").append(data);
							$(".appArea").data(req,data);
						}
					});
				}
		    }			
		</script>     
                    <div id="sidebar" class="nano has-scrollbar">
                        <ul class="menu content" tabindex="0" style="right: -17px;">
                            <li>
                            <h3>云主机</h3>
                                <ul>
                                    <li><a href="#" wp-type="link" class="selected" onclick="invoke('${ctx}v_right');setAselected(this)"><i class="overview"></i><span>
                                        概览</span></a></li>
                                    <li><a onclick="invoke(  ' ${ctx}admin/v_cloudhost');setAselected(this)" href="#" wp-type="link" class=""><i class="instance"></i><span>
                                        云主机</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#" wp-type="link"><i class="volume"></i><span>
                                        云硬盘</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="snapshot"></i><span>
                                        快照</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"   wp-type="link"><i class="image"></i><span>
                                        镜像</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="ip"></i><span>
                                        公网IP</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="security"></i><span>
                                        安全组</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#" wp-type="link"><i class="key-pair"></i><span>
                                        密钥对</span></a></li>
                                </ul>
                            </li>
                            <li>
                            <h3>虚拟数据中心</h3>
                                <ul>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link" class="" ><i class="topology"></i><span>
                                            网络拓扑</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="router"></i><span>
                                            路由器</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="network"></i><span>
                                            网络</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="interface"></i><span>
                                            虚拟网卡</span></a></li>
                                </ul>
                            </li>
                            <li>
                            <h3>用户中心</h3>
                                <ul>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="record_overview"></i><span>
                                            消费记录</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="ticket"></i><span>
                                            工单支持</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="operations"></i><span>
                                            操作日志</span></a></li>
                                </ul>
                            </li>
                            <li>
                            <h3>用户中心</h3>
                                <ul>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="record_overview"></i><span>
                                            消费记录</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="ticket"></i><span>
                                            工单支持</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="operations"></i><span>
                                            操作日志</span></a></li>
                                </ul>
                            </li>
                            <li>
                            <h3>用户中心</h3>
                                <ul>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="record_overview"></i><span>
                                            消费记录</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="ticket"></i><span>
                                            工单支持</span></a></li>
                                    <li><a onclick="invoke( ' ${ctx}admin/v_cloudHD');setAselected(this)" href="#"  wp-type="link"><i class="operations"></i><span>
                                            操作日志</span></a></li>
                                </ul>
                            </li>                            
                        </ul>
                    <div class="pane" style="display: none;"><div class="slider" style="height: 688px; -webkit-transform: translate(0px, 0px);"></div></div>
                    </div>
<script type="text/javascript">
function setAselected(currentA){
	var alinks=$("a[wp-type=link]");
	for(var i=0;i<alinks.length;i++){
		if(alinks[i]!=currentA){
			//alinks[i]的class=""
			alinks[i].className="";
		}else{
			//当前A的class="selected"
			currentA.className="selected";
		}
	}
}
</script>