/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50022
Source Host           : localhost:3306
Source Database       : console

Target Server Type    : MYSQL
Target Server Version : 50022
File Encoding         : 65001

Date: 2014-08-19 11:26:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `console_user`
-- ----------------------------
DROP TABLE IF EXISTS `console_user`;
CREATE TABLE `console_user` (
  `user_id` int(11) NOT NULL auto_increment,
  `user_name` varchar(30) default NULL,
  `pwd` varchar(20) default NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of console_user
-- ----------------------------
INSERT INTO `console_user` VALUES ('1', 'admin', 'admin');
