package com.doadway.resource.web.controller;

import java.net.MalformedURLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.caucho.hessian.client.HessianProxyFactory;
import com.doadway.framework.web.webservice.HessianService;
@Controller
public class HessianController {
	@RequestMapping("/sayHello")
	public  String sayHello(HttpServletRequest request,HttpServletResponse response)  {
		  String url = "http://localhost:8080/console/remote/hessianService";  
		  HessianProxyFactory factory = new HessianProxyFactory();  
		  HessianService hessianServer;
		  
		 try {
				hessianServer = (HessianService)factory.create(HessianService.class, url);
				String ret = hessianServer.sayHello("zhoutanxin");  
				System.out.println(ret);  		
		} catch (MalformedURLException e) {
				e.printStackTrace();
		}
		 
        return "redirect:/admin/v_cloudhost"; 		
	}
	
}
