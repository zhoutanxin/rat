package com.doadway.resource.web.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
public class CommonController {
	@RequestMapping("/v_top")
	public  String v_top()  {
        return "include/top"; 		
	}
	
	@RequestMapping("/v_left")
	public  String v_left(HttpServletRequest request,HttpServletResponse response)  {
        return "include/left_sidebar"; 		
	}
	@RequestMapping("/v_right")
	public  String v_right(HttpServletRequest request,HttpServletResponse response)  {
		return "include/right_app"; 		
	}
	@RequestMapping("/welcome")
	public  String v_welcome(HttpServletRequest request,HttpServletResponse response)  {
		return "index/welcome"; 		
	}
	@RequestMapping("/admin/vm_view")
	public  String vm_view(HttpServletRequest request,HttpServletResponse response,Map<String,Object> map)  {
		map.put("info","this is vm view!!");
		return "index.vm"; 		
	}
}
