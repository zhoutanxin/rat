package com.doadway.resource.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
@RequestMapping("/admin")
public class UserController {
	@RequestMapping("/v_cloudhost")
	public  String v_cloudhost(HttpServletRequest request,HttpServletResponse response)  {
        return "cloud/cloudhost"; 		
	}
	
	@RequestMapping("/v_cloudHD")
	public  String v_cloudHD(HttpServletRequest request,HttpServletResponse response)  {
        return "cloud/cloudHD"; 		
	}
}
