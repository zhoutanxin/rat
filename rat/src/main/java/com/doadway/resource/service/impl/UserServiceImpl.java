package com.doadway.resource.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.doadway.resource.dao.UserMapper;
import com.doadway.resource.pojo.User;
import com.doadway.resource.pojo.UserExample;
import com.doadway.resource.pojo.UserExample.Criteria;
import com.doadway.resource.service.UserService;
@Service
public class UserServiceImpl implements UserService {
	@Resource
	UserMapper userDAO;
	@Override
	public boolean saveUser(User user) {
		return userDAO.insert(user)>0;
	}
	
	@Override
	public User findByUserName(String userName) {
		UserExample example =new UserExample();
		Criteria userCri=example.createCriteria();
		userCri.andUserNameEqualTo(userName);
		if(userDAO.selectByExample(example).size()>0) return userDAO.selectByExample(example).get(0);
		return null;
	}

	public UserMapper getUserDAO() {
		return userDAO;
	}
	public void setUserDAO(UserMapper userDAO) {
		this.userDAO = userDAO;
	}

	
}
