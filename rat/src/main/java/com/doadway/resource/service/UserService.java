package com.doadway.resource.service;

import com.doadway.resource.pojo.User;

public interface UserService {
	public boolean saveUser(User user);
	
	public User findByUserName(String userName);
}
