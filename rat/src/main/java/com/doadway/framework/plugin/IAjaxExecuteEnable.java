package com.doadway.framework.plugin;

/**
 * 可被异步调用
 * @author kingapex
 *
 */
public interface IAjaxExecuteEnable {
	
	public String execute();
	
}
