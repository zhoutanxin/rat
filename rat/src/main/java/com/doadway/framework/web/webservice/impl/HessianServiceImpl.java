package com.doadway.framework.web.webservice.impl;

import com.doadway.framework.web.webservice.HessianService;
public class HessianServiceImpl implements HessianService {

	private static final long serialVersionUID = -6922475471566625967L;

	@Override
	public String sayHello(String username) {
		return "Hello, " + username;  
	}

}
